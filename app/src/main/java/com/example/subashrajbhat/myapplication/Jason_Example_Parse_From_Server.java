package com.example.subashrajbhat.myapplication;

import android.app.Activity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by subash rajbhat on 5/21/2015.
 */
public class Jason_Example_Parse_From_Server extends Activity  implements View.OnClickListener {
    private TextView etJonsonInput;
    private Button parsButton;
    private TextView etOutput;
    //private String inputData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.jasonexample);
        etJonsonInput = (TextView) findViewById(R.id.edttext);
        parsButton = (Button) findViewById(R.id.btn_parse);
        etOutput = (TextView) findViewById(R.id.textviewoutput);

        parsButton.setOnClickListener(this);
        // arko method string call garna

        //Log.d("testing",inputData);
        //string banaya ra method call garako
        //etJonsonInput.setText(inputData);

    }



    @Override
    public void onClick(View v) {
        if(Utils.isNetworkConnected(this)) {// yo code laya network access check garako

            String url = "http://www.json-generator.com/api/json/get/cqKwksGEfc?indent=2";
            DataFetcher dataFetcher = new DataFetcher(Jason_Example_Parse_From_Server.this);//datafetcher vanako  class ko name ho
            dataFetcher.execute(url);
        }
        else{
            Toast.makeText(this,"please check your internet connection",Toast.LENGTH_LONG).show();
        }


    }
    //this method is used by datafetcherAsyncTask to pass the server response
    //whwen server data fetching is completed
    //@param serverResponse the response from the server

    public void parseJsonData(String serverResponse) {//jason object banauna lagako
        if (TextUtils.isEmpty(serverResponse))//server empty vaya message dine banako
        {
            etOutput.setText("no data");

        } else {
            etJonsonInput.setText(serverResponse);
            try {//yo bujhna copy ko format hernu parcha json ko first example
                JSONObject mainJsonObject = new JSONObject(serverResponse);
                //Extracting name object from the main json object
                JSONObject nameJsonObject = mainJsonObject.getJSONObject("name"); //one is the name of the key in file or server
                //getting the first name from the name jsonObject
                String firstName = nameJsonObject.getString("first_name");//getting last
                //getting the last name from the name jsonobject
                String lastName = nameJsonObject.getString("last_name");
                //getting address
                String address = mainJsonObject.getString("address");

                //get the study json array from the main jsonObject
                JSONArray studyJsonArray = mainJsonObject.getJSONArray("study");//getting the study from the array of jason

                int lengthOfStudyJsonArray = studyJsonArray.length();// getting the length of   array from the main jsonobject
                //loop till the length of the array
                String[] study = new String[lengthOfStudyJsonArray];//
                for (int i = 0; i < lengthOfStudyJsonArray; i++) {
                    //get the study JsonObject from the study JsonArray

                    JSONObject studyJsonObject = studyJsonArray.getJSONObject((i));
                    //getting the institute from the study jsonObject
                    String institute = studyJsonObject.getString(("institute"));
                    String level = studyJsonObject.getString("level");

                    study[i] = level + "from" + institute;//string concatination garako
                }
                    String studyCombine="";
                    for(int i=0;i<lengthOfStudyJsonArray;i++) {
                        studyCombine += study[i] + "\n";
                    }

                    String resultToDisplay = firstName+""+lastName+"\n"+address+"\n"+studyCombine;// result display garako
                etOutput.setText(resultToDisplay);



            } catch (JSONException e) {
                e.printStackTrace();
                etOutput.setText("Json exception occured");



            }


        }
    }
}
