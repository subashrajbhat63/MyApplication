package com.example.subashrajbhat.myapplication;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

/**
 * Created by subash rajbhat on 5/22/2015.
 */
public class JasonChoice  extends Activity implements View.OnClickListener {
    private Button btnParseFromFile, btnParseFromServer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.jasonchoice);
        btnParseFromFile = (Button) findViewById(R.id.btn_from_file);
        btnParseFromServer = (Button) findViewById(R.id.btn_from_server);
      btnParseFromFile.setOnClickListener(this);
        btnParseFromServer.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        if (v.getId() == btnParseFromFile.getId()) {//v vanako view ma use vaya ko  ho
            Intent intent = new Intent(JasonChoice.this,Jason_Example.class);
            startActivity(intent);


        } else if (v.getId() == btnParseFromServer.getId()) {
            Intent intent=new Intent(JasonChoice.this,Jason_Example_Parse_From_Server.class);
            startActivity(intent);
        }
    }
}

