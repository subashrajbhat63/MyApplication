package com.example.subashrajbhat.myapplication;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

//import all the buttons and changes made in the project

public class MainActivity extends Activity implements View.OnClickListener, View.OnLongClickListener {

//creating the class of the gui used
    private TextView textViewDisplay;
    private EditText editTextInput; //agadi ko class name ho pachadi ko hamilaya dine nam ho
    private Button buttonEnter;
    private Button btnNextActivity;


//giving the function of each part
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.simple_form_eg);
        editTextInput = (EditText) findViewById(R.id.editText);
        textViewDisplay = (TextView) findViewById(R.id.textview);//text view ma kam garako
        buttonEnter = (Button) findViewById(R.id.button);
        buttonEnter.setOnClickListener(this);//button ko effect rakhako
        buttonEnter.setOnLongClickListener(this);
      btnNextActivity=(Button)findViewById(R.id.button2); //name=(class nam) findview....(r.id.button nam)

       btnNextActivity.setOnClickListener(this);//(name.effect(this))




    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.



        // changing the activity to main menu
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
// displaying the result in the editinput
    @Override
    public void onClick(View v) {
        //to make button seperate with the id  and do the individual work
        switch (v.getId()){//switch case bata select garako choice anusar if else lagauna sakincha
            case R.id.button:
              displayresult();
                break;

           //to go to the second class
            case R.id.button2:
                Intent intent=new Intent(MainActivity.this,JasonChoice.class);//next page call garako kun page ma jane vanaya ra
                intent.putExtra("name",editTextInput.getText().toString());//arko page ma message pathauna intent ma halaya ra pathako


                startActivity(intent);// yo laya activity start agarako



        }




    }

    private void displayresult() {

        Log.d("MainActivity", "Button is Clicked");
        String input = editTextInput.getText().toString();
        textViewDisplay.setText("Hello" + input);
        btnNextActivity.setVisibility(View.VISIBLE);// button visible garako

    }

    //use of toast in the program
    @Override
    public boolean onLongClick(View v) {
        Toast.makeText(this,"hello user",Toast.LENGTH_LONG).show();
        return true;
    }
}
