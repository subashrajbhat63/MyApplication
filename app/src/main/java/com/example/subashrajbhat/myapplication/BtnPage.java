package com.example.subashrajbhat.myapplication;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Created by subash rajbhat on 5/22/2015.
 */
public class BtnPage  extends Activity implements View.OnClickListener {
    private Button btnjason;
    private Button btnfirsteg;
    private Button exit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.btnpage);

        btnjason = (Button) findViewById(R.id.buttonjason);
        btnjason.setOnClickListener(this);//button ko effect rakhako

     btnfirsteg=(Button)findViewById(R.id.buttonsimple);
        btnfirsteg.setOnClickListener(this);//name=(class nam) findview....(r.id.button nam)


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.buttonjason: {
                Intent intent1 = new Intent(BtnPage.this, JasonChoice.class);
                startActivity(intent1);
            }//switch case bata select garako choice anusar if else lagauna sakincha
            case R.id.buttonsimple:{
                Intent intent = new Intent(BtnPage.this, MainActivity.class);

                startActivity(intent);}


                //to go to the second class
            //next page call garako kun page ma jane vanaya ra
                //intent.putExtra("name",editTextInput.getText().toString());//arko page ma message pathauna intent ma halaya ra pathako



            case R.id.buttonexit:
            finish();
                break;
        }

    }
}
