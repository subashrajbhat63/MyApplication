package com.example.subashrajbhat.myapplication;

import android.os.AsyncTask;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.io.IOException;

/**
 * Created by subash rajbhat on 5/21/2015.
 */
public class DataFetcher extends AsyncTask  <String,Void,String> {
    private Jason_Example_Parse_From_Server activity;
    DataFetcher(Jason_Example_Parse_From_Server activity){
        this.activity=activity;
    }

    //url lai implement garako i.i pass garako arko method to fetch
    /*String url;
    public DataFetcher(String url){
        this.url=url;
    }*/
    @Override
    //asyntask ko third parameter  yes ko return type
    protected String doInBackground(String... params) {
        //fetching the url sent from asyncTask.execute(url)
        String url = params[0];
        String serverResponse;

        try {
            //yo kura jasai banau na parcha net connection garna ra jaha chaincha tyai implement garne
            //
            DefaultHttpClient httpClient=new DefaultHttpClient();
            HttpEntity httpEntity;
            HttpResponse httpResponse;
            HttpGet httpGet=new HttpGet(url);
            httpResponse= httpClient.execute(httpGet);//httpget lai serer hit garako httpclient garaya ra
            httpEntity=httpResponse.getEntity();
            serverResponse= EntityUtils.toString((httpEntity));//response ayo yaha sama
        } catch (IOException e) {
            e.printStackTrace();
            serverResponse=null;
        }
        return serverResponse;
    }

    @Override
    //doinbackground bata response liya ra on post ma execute garako
    protected void onPostExecute(String serverResponse) {
        activity.parseJsonData(serverResponse);


    }
  /* @Override
    //downloading progress dekhauna rakhako method
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
    }*/

    /*@Override
    paila kinai data ako cha vanaya test garna

    protected void onPreExecute() {
        super.onPreExecute();
    }*/
}

