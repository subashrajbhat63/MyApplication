package com.example.subashrajbhat.myapplication;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by subash rajbhat on 5/22/2015.
 */
//yo class chai net connection check garna kam lag cha
    //checks if the device is connected to network or not
    //@contect Activity/content where we want to check the internet connection
    //return true if n/w is available
public class Utils {
    public static boolean isNetworkConnected(Context context ){
        ConnectivityManager connMgr=(ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo=connMgr.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }
}
